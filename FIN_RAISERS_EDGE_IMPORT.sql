CREATE OR REPLACE PACKAGE FIN_RAISERS_EDGE_IMPORT IS
/*------------------------------------------------------------------------------
PROJECT: TCCOLUMBIA-SOW108-GRANTBILLING
PACKAGE: FIN_RAISERS_EDGE_IMPORT
DESCRIPTION:
This package contains any functions and procedures necessary to support Raisers
Edge gift/grant data import into Banner.

PREREQUISITES/DEPENDENCIES:

An Oracle directory must exist and be defined in order to put the tab-delimited
Raisers Edge files into.

A process must be defined to copy Raisers Edge flat files to the Oracle
directory specified by the external table.

A process must be defined to fixup Raisers Edge flat files to loop through all
available import files, rename them to the filename specified in the 
re_import_ext external table.  After each file is processed, it should be 
renamed again to include the date and timestamp of processing and archived
according to specification.

AUTHOR:  David Gilmore, Strata Information Group
VERSION:  1.0, 08/30/2018
AUDIT TRAIL:
------------------------------------------------------------------------------*/
  
  -----------------------------------------------------------------------------
  -- Main procedure to import data from the external table re_import_ext.
  -- For each record, determine if the record is a gift or grant.
  -- If it is a grant record, insert to TRRACCD.
  -- If it is a gift record, insert to GURFEED.
  -- All grant records begin with an index and fund value of '5'.
  -- The grant code is fetched from FTVFUND for insert to TRRACCD.
  -----------------------------------------------------------------------------
  PROCEDURE P_IMPORT_FROM_EXT_TABLE;

END FIN_RAISERS_EDGE_IMPORT;

/


CREATE OR REPLACE PACKAGE BODY FIN_RAISERS_EDGE_IMPORT IS
/*------------------------------------------------------------------------------
PROJECT: TCCOLUMBIA-SOW108-GRANTBILLING
PACKAGE: FIN_RAISERS_EDGE_IMPORT
DESCRIPTION:
This package contains any functions and procedures necessary to support Raisers
Edge gift/grant data import into Banner.

PREREQUISITES/DEPENDENCIES:

An Oracle directory must exist and be defined in order to put the tab-delimited
Raisers Edge files into.

A process must be defined to copy Raisers Edge flat files to the Oracle
directory specified by the external table.

A process must be defined to fixup Raisers Edge flat files to loop through all
available import files, rename them to the filename specified in the 
re_import_ext external table.  After each file is processed, it should be 
renamed again to include the date and timestamp of processing and archived
according to specification.

AUTHOR:  David Gilmore, Strata Information Group
VERSION:  1.0, 08/30/2018
AUDIT TRAIL:
------------------------------------------------------------------------------*/

  --global variables for insert/delete/error counts
  v_err_count        NUMBER(8)                        := 0;
  v_gur_ins_count    NUMBER(8)                        := 0;
  v_gur_del_count    NUMBER(8)                        := 0;
  v_trr_ins_count    NUMBER(8)                        := 0;
  v_trr_del_count    NUMBER(8)                        := 0;
  v_tot_ins_count    NUMBER(8)                        := 0;

-----------------------------------------------------------------------------
-- Function to validate numeric values from Raisers Edge
-----------------------------------------------------------------------------  
FUNCTION f_convert_number(pi_string IN VARCHAR2) 
RETURN NUMBER
IS

  lv_return NUMBER;
  
BEGIN
  
  SELECT REGEXP_REPLACE(pi_string, '[^0-9A-Za-z]', '')
    INTO lv_return 
    FROM dual;
  
  RETURN lv_return;
  
EXCEPTION 
  WHEN OTHERS THEN
  
  dbms_output.put_line('Error converting ' || pi_string || ' to number. ' || SQLERRM);
  
  v_err_count := v_err_count + 1;
  
  RETURN NULL;
  
END f_convert_number;

-----------------------------------------------------------------------------
-- Function to validate date values from Raisers Edge
-- Currently, there appears to be only two incoming formats to account for.
-----------------------------------------------------------------------------  
FUNCTION f_convert_date(pi_string IN VARCHAR2) 
RETURN DATE
IS

  lv_return   DATE             := NULL;
  lv_string   VARCHAR2(50);
  
BEGIN
  
  lv_string := TRIM(pi_string);
  
  IF length(lv_string) = 14 THEN
     --format is YYYYMMDDHH24MISS
     
     lv_return := TO_DATE(lv_string,'YYYYMMDDHH24MISS');
     
  END IF;
  
  IF length(lv_string) = 9 THEN
     --format is DD-MON-YY
     
     lv_return := TO_DATE(lv_string,'DD-MON-YY');
     
  END IF;
  
  RETURN lv_return;
  
EXCEPTION 
  WHEN OTHERS THEN
  
  dbms_output.put_line('Error converting ' || pi_string || ' to date. ' || SQLERRM);
  
  v_err_count := v_err_count + 1;
  
  RETURN NULL;
  
END f_convert_date;

-----------------------------------------------------------------------------
-- Function to fetch grant code from FTVFUND using fund code
-----------------------------------------------------------------------------  
FUNCTION f_get_grnt_code(pi_fund_code IN VARCHAR2) 
RETURN VARCHAR2
IS

  lv_return   trraccd.trraccd_grnt_code%TYPE    := NULL;
  
BEGIN
  
  SELECT MAX(f.ftvfund_grnt_code)
    INTO lv_return
    FROM ftvfund f
   WHERE f.ftvfund_fund_code = pi_fund_code
     AND f.ftvfund_coas_code = '1'
     AND f.ftvfund_nchg_date = (SELECT MAX(d.ftvfund_nchg_date)
                                  FROM ftvfund d
                                 WHERE d.ftvfund_fund_code = f.ftvfund_fund_code
                                   AND d.ftvfund_coas_code = f.ftvfund_coas_code);
  
  IF lv_return IS NULL THEN
    
     dbms_output.put_line('Failed to fetch a valid grant code using ' || pi_fund_code || '. ');
  
     v_err_count := v_err_count + 1;
    
  END IF;
  
  RETURN lv_return;
  
EXCEPTION 
  WHEN OTHERS THEN
  
  dbms_output.put_line('Error fetching grant code using ' || pi_fund_code || '. ' || SQLERRM);
  
  v_err_count := v_err_count + 1;
  
  RETURN NULL;
  
END f_get_grnt_code;

-----------------------------------------------------------------------------
-- Function to fetch tran number
-----------------------------------------------------------------------------  
FUNCTION f_get_tran_number(pi_grnt_code IN trraccd.trraccd_grnt_code%type) 
RETURN NUMBER
IS

  lv_return   trraccd.trraccd_tran_number%TYPE    := NULL;
  
BEGIN
  
  SELECT nvl(MAX(trraccd.trraccd_tran_number), 0) + 1
    INTO lv_return
    FROM trraccd
   WHERE trraccd.trraccd_grnt_code = pi_grnt_code;
  
  RETURN lv_return;
  
EXCEPTION 
  WHEN OTHERS THEN
    
  dbms_output.put_line('Error fetching tran number using ' || pi_grnt_code || 
                       ' grant code. ' || SQLERRM);
  
  v_err_count := v_err_count + 1;
  
  RETURN NULL;
  
END f_get_tran_number;

-----------------------------------------------------------------------------
-- Procedure to delete from GURFEED
-- No API available
-- Trap any error that occurs
-----------------------------------------------------------------------------
PROCEDURE p_delete_from_gurfeed
IS

  lv_count     NUMBER(8)     := 0;
  lv_total     NUMBER(8)     := 0;

  CURSOR c_get_doc_code IS
  SELECT DISTINCT r.doc_code
    FROM re_import_ext r;

BEGIN
  
  FOR doc_rec IN c_get_doc_code LOOP
    
      SELECT COUNT(*)
        INTO lv_count
        FROM gurfeed
       WHERE gurfeed.gurfeed_doc_code = doc_rec.doc_code
         AND gurfeed.gurfeed_system_id = 'RAISERS';
  
      DELETE FROM gurfeed
       WHERE gurfeed.gurfeed_doc_code = doc_rec.doc_code
         AND gurfeed.gurfeed_system_id = 'RAISERS';
         
      lv_total := lv_total + lv_count;
     
  END LOOP;
  
  v_gur_del_count := lv_total;
     
  COMMIT;
  
EXCEPTION
  WHEN OTHERS THEN
    dbms_output.put_line('Error during p_delete_from_gurfeed: ' || SQLERRM);
    
END p_delete_from_gurfeed;

-----------------------------------------------------------------------------
-- Procedure to insert a single row into GURFEED
-- No API available
-- Trap any error that occurs
-----------------------------------------------------------------------------
PROCEDURE p_insert_to_gurfeed (pi_system_id           gurfeed.gurfeed_system_id%TYPE,
                               pi_system_time_stamp   gurfeed.gurfeed_system_time_stamp%TYPE,
                               pi_doc_code            gurfeed.gurfeed_doc_code%TYPE,
                               pi_rec_type            gurfeed.gurfeed_rec_type%TYPE,
                               pi_submission_number   gurfeed.gurfeed_submission_number%TYPE,
                               pi_item_num            gurfeed.gurfeed_item_num%TYPE,
                               pi_seq_num             gurfeed.gurfeed_seq_num%TYPE,
                               pi_activity_date       gurfeed.gurfeed_activity_date%TYPE,
                               pi_user_id             gurfeed.gurfeed_user_id%TYPE,
                               pi_rucl_code           gurfeed.gurfeed_rucl_code%TYPE,
                               pi_doc_ref_num         gurfeed.gurfeed_doc_ref_num%TYPE,
                               pi_trans_date          gurfeed.gurfeed_trans_date%TYPE,
                               pi_trans_amt           gurfeed.gurfeed_trans_amt%TYPE,
                               pi_trans_desc          gurfeed.gurfeed_trans_desc%TYPE,
                               pi_dr_cr_ind           gurfeed.gurfeed_dr_cr_ind%TYPE,
                               pi_coas_code           gurfeed.gurfeed_coas_code%TYPE,
                               pi_acci_code           gurfeed.gurfeed_acci_code%TYPE,
                               pi_fund_code           gurfeed.gurfeed_fund_code%TYPE,
                               pi_orgn_code           gurfeed.gurfeed_orgn_code%TYPE,
                               pi_acct_code           gurfeed.gurfeed_acct_code%TYPE,
                               pi_prog_code           gurfeed.gurfeed_prog_code%TYPE,
                               pi_vendor_pidm         gurfeed.gurfeed_vendor_pidm%TYPE)							   
IS
BEGIN

  INSERT INTO GURFEED (GURFEED_SYSTEM_ID        ,
                       GURFEED_SYSTEM_TIME_STAMP,
                       GURFEED_DOC_CODE         ,
                       GURFEED_REC_TYPE         ,
                       GURFEED_SUBMISSION_NUMBER,
                       GURFEED_ITEM_NUM         ,
                       GURFEED_SEQ_NUM          ,
                       GURFEED_ACTIVITY_DATE    ,
                       GURFEED_USER_ID          ,
                       GURFEED_RUCL_CODE        ,
                       GURFEED_DOC_REF_NUM      ,
                       GURFEED_TRANS_DATE       ,
                       GURFEED_TRANS_AMT        ,
                       GURFEED_TRANS_DESC       ,
                       GURFEED_DR_CR_IND        ,
                       GURFEED_COAS_CODE        ,
                       GURFEED_ACCI_CODE        ,
                       GURFEED_FUND_CODE        ,
                       GURFEED_ORGN_CODE        ,
                       GURFEED_ACCT_CODE        ,
                       GURFEED_PROG_CODE        ,
                       GURFEED_VENDOR_PIDM)
			         VALUES (pi_system_id        ,
                       pi_system_time_stamp,
                       pi_doc_code         ,
                       pi_rec_type         ,
                       pi_submission_number,
                       pi_item_num         ,
                       pi_seq_num          ,
                       pi_activity_date    ,
                       pi_user_id          ,
                       pi_rucl_code        ,
                       pi_doc_ref_num      ,
                       pi_trans_date       ,
                       pi_trans_amt        ,
                       pi_trans_desc       ,
                       pi_dr_cr_ind        ,
                       pi_coas_code        ,
                       pi_acci_code        ,
                       pi_fund_code        ,
                       pi_orgn_code        ,
                       pi_acct_code        ,
                       pi_prog_code        ,
                       pi_vendor_pidm);
     
  COMMIT;
  
  v_gur_ins_count := v_gur_ins_count + 1;
  
EXCEPTION
  WHEN OTHERS THEN
    dbms_output.put_line('Error during p_insert_to_gurfeed: ' || SQLERRM);
    
    v_err_count := v_err_count + 1;
    
END p_insert_to_gurfeed;

-----------------------------------------------------------------------------
-- Procedure to delete from TRRACCD
-- No API available
-- Trap any error that occurs
-- (not used)
-----------------------------------------------------------------------------
PROCEDURE p_delete_from_trraccd
IS

  lv_count     NUMBER(8)      := 0;
  lv_total     NUMBER(8)      := 0;

  CURSOR c_get_doc_code IS
  SELECT DISTINCT r.doc_code
    FROM re_import_ext r;

BEGIN
  
  FOR doc_rec IN c_get_doc_code LOOP
    
      SELECT COUNT(*)
        INTO lv_count
        FROM trraccd
       WHERE trraccd.trraccd_desc = doc_rec.doc_code
         AND trraccd.trraccd_detail_code = 'GRSR'; 
      
      DELETE FROM trraccd
       WHERE trraccd.trraccd_desc = doc_rec.doc_code
         AND trraccd.trraccd_detail_code = 'GRSR'; 
         
      lv_total := lv_total + lv_count;
     
  END LOOP;
     
  COMMIT;
  
  v_trr_del_count := lv_total;
  
EXCEPTION
  WHEN OTHERS THEN
    dbms_output.put_line('Error during p_delete_from_trraccd: ' || SQLERRM);
    
    v_err_count := v_err_count + 1;
    
END p_delete_from_trraccd;

-----------------------------------------------------------------------------
-- Procedure to insert a single row into TRRACCD
-- No API available
-- Trap any error that occurs
-----------------------------------------------------------------------------
PROCEDURE p_insert_to_trraccd (pi_grnt_code         trraccd.trraccd_grnt_code%TYPE,
                               pi_tran_number       trraccd.trraccd_tran_number%TYPE,
                               pi_coas_code         trraccd.trraccd_coas_code%TYPE,
                               pi_fund_code         trraccd.trraccd_fund_code%TYPE,
                               pi_detail_code       trraccd.trraccd_detail_code%TYPE,
                               pi_trans_date        trraccd.trraccd_trans_date%TYPE,
                               pi_effective_date    trraccd.trraccd_effective_date%TYPE,
                               pi_amount            trraccd.trraccd_amount%TYPE,
                               pi_balance           trraccd.trraccd_balance%TYPE,
                               pi_srce_code         trraccd.trraccd_srce_code%TYPE,
                               pi_acct_feed_ind     trraccd.trraccd_acct_feed_ind%TYPE,
                               pi_session_number    trraccd.trraccd_session_number%TYPE,
                               pi_activity_date     trraccd.trraccd_activity_date%TYPE,
                               pi_user_id           trraccd.trraccd_user_id%TYPE,
                               pi_desc              trraccd.trraccd_desc%TYPE,
                               pi_entry_date		trraccd.trraccd_entry_date%TYPE)
IS
BEGIN

  INSERT INTO TRRACCD (TRRACCD_GRNT_CODE     ,
                       TRRACCD_TRAN_NUMBER   ,
                       TRRACCD_COAS_CODE     ,
                       TRRACCD_FUND_CODE     ,
                       TRRACCD_DETAIL_CODE   ,
                       TRRACCD_TRANS_DATE    ,
                       TRRACCD_EFFECTIVE_DATE,
                       TRRACCD_AMOUNT        ,
                       TRRACCD_BALANCE       ,
                       TRRACCD_SRCE_CODE     ,
                       TRRACCD_ACCT_FEED_IND ,
                       TRRACCD_SESSION_NUMBER,
                       TRRACCD_ACTIVITY_DATE ,
                       TRRACCD_USER_ID       ,
                       TRRACCD_DESC          ,
                       TRRACCD_ENTRY_DATE)
			         VALUES (pi_grnt_code     ,
                       pi_tran_number   ,
                       pi_coas_code     ,
                       pi_fund_code     ,
                       pi_detail_code   ,
                       pi_trans_date    ,
                       pi_effective_date,
                       pi_amount        ,
                       pi_balance       ,
                       pi_srce_code     ,
                       pi_acct_feed_ind ,
                       pi_session_number,
                       pi_activity_date ,
                       pi_user_id       ,
                       pi_desc          ,
                       pi_entry_date);
                       
  COMMIT;
  
  v_trr_ins_count := v_trr_ins_count + 1;
                       
EXCEPTION
  WHEN OTHERS THEN
    dbms_output.put_line('Error during p_insert_to_trraccd: ' || SQLERRM);
    
    v_err_count := v_err_count + 1;
    
END p_insert_to_trraccd;

-----------------------------------------------------------------------------
-- Function to adjust header record amount to subtract the grant amounts.
-----------------------------------------------------------------------------
FUNCTION f_adjust_header_amt (pi_amount         gurfeed.gurfeed_trans_amt%TYPE,
                              pi_doc_code       gurfeed.gurfeed_doc_code%TYPE)
RETURN NUMBER
IS

  lv_return     gurfeed.gurfeed_trans_amt%TYPE   := 0;
  lv_grant_amt  gurfeed.gurfeed_trans_amt%TYPE   := 0;
  lv_grant_tot  gurfeed.gurfeed_trans_amt%TYPE   := 0;
  
  CURSOR c_get_grant_recs IS
  SELECT *
    FROM re_import_ext r
   WHERE r.rec_type <> '1'
     AND SUBSTR(r.acci_code,1,1) = '5'
     AND SUBSTR(r.fund_code,1,1) = '5'
     AND r.doc_code = pi_doc_code;
  
BEGIN
  
  FOR grant_rec IN c_get_grant_recs LOOP
    
      lv_grant_amt := f_convert_number(grant_rec.trans_amt);
      
      lv_grant_tot := lv_grant_tot + lv_grant_amt;
    
  END LOOP;
  
  lv_return := pi_amount - lv_grant_tot;
  
  RETURN lv_return;
  
EXCEPTION
  WHEN OTHERS THEN
    dbms_output.put_line('Error during f_adjust_header_amt: ' || SQLERRM);
    
    v_err_count := v_err_count + 1;
    
    RETURN NULL;
    
END f_adjust_header_amt;

-----------------------------------------------------------------------------
-- Main procedure to import data from the external table re_import_ext.
-- For each record, determine if the record is a gift or grant.
-- If it is a grant record, insert to TRRACCD.
-- If it is a gift record, insert to GURFEED.
-- All grant records begin with an index and fund value of '5'.
-- The grant code is fetched from FTVFUND for insert to TRRACCD.
-----------------------------------------------------------------------------
PROCEDURE P_IMPORT_FROM_EXT_TABLE
IS

  --Variables to hold converted/default values from the raw data in re_import_ext
  lv_system_id           gurfeed.gurfeed_system_id%TYPE              := 'RAISERS';
  lv_system_time_stamp   gurfeed.gurfeed_system_time_stamp%TYPE      := NULL;
  lv_doc_code            gurfeed.gurfeed_doc_code%TYPE               := NULL;
  lv_rec_type            gurfeed.gurfeed_rec_type%TYPE               := NULL;
  lv_submission_number   gurfeed.gurfeed_submission_number%TYPE      := 0;
  lv_item_num            gurfeed.gurfeed_item_num%TYPE               := 0;
  lv_seq_num             gurfeed.gurfeed_seq_num%TYPE                := NULL;
  lv_activity_date       gurfeed.gurfeed_activity_date%TYPE          := NULL;
  lv_user_id             gurfeed.gurfeed_user_id%TYPE                := NULL;
  lv_rucl_code           gurfeed.gurfeed_rucl_code%TYPE              := NULL;
  lv_doc_ref_num         gurfeed.gurfeed_doc_ref_num%TYPE            := NULL;
  lv_trans_date          gurfeed.gurfeed_trans_date%TYPE             := NULL;
  lv_trans_amt           gurfeed.gurfeed_trans_amt%TYPE              := NULL;
  lv_trans_desc          gurfeed.gurfeed_trans_desc%TYPE             := NULL;
  lv_dr_cr_ind           gurfeed.gurfeed_dr_cr_ind%TYPE              := NULL;
  lv_coas_code           gurfeed.gurfeed_coas_code%TYPE              := '1';
  lv_acci_code           gurfeed.gurfeed_acci_code%TYPE              := NULL;
  lv_fund_code           gurfeed.gurfeed_fund_code%TYPE              := NULL;
  lv_orgn_code           gurfeed.gurfeed_orgn_code%TYPE              := NULL;
  lv_acct_code           gurfeed.gurfeed_acct_code%TYPE              := NULL;
  lv_prog_code           gurfeed.gurfeed_prog_code%TYPE              := NULL;
  lv_vendor_pidm         gurfeed.gurfeed_vendor_pidm%TYPE            := NULL;
  
  --variables for TRRACCD conversion
  lv_grnt_code           trraccd.trraccd_grnt_code%TYPE              := NULL;
  lv_tran_number         trraccd.trraccd_tran_number%TYPE            := NULL;
  lv_detail_code         trraccd.trraccd_detail_code%TYPE            := 'GRSR';
  lv_balance             trraccd.trraccd_balance%TYPE                := NULL;
  lv_srce_code           trraccd.trraccd_srce_code%TYPE              := 'J';
  lv_acct_feed_ind       trraccd.trraccd_acct_feed_ind%TYPE          := 'Y';
  lv_session_number      trraccd.trraccd_session_number%TYPE         := 0;

  --used to check to see if the record has already been processed
  lv_count_fgbtrni            NUMBER(8)               := 0;
  lv_count_fgbjvch            NUMBER(8)               := 0;
  lv_count_fgbtrnh            NUMBER(8)               := 0;
  
  lv_count                    NUMBER(8)               := 0;

  --cursor for all import records
  CURSOR c_get_re_import_recs IS
  SELECT *
    FROM re_import_ext;

BEGIN
  
  --reset global variables
  v_err_count     := 0;
  v_gur_ins_count := 0;
  v_gur_del_count := 0;
  v_trr_ins_count := 0;
  v_trr_del_count := 0;
  v_tot_ins_count := 0;
  
  --For safety's sake, try to delete any pre-existing transactions from
  --GURFEED.  There should be only one set per doc code.
  --Doc code is being stored in the description field for TRRACCD.
  --If the doc code exists in TRRACCD, then don't attempt a re-insert or delete
  p_delete_from_gurfeed;
  
  --Loop through all records in the external table and process accordingly
  FOR import_rec IN c_get_re_import_recs LOOP
    
      --Convert values in the external table to their corresponding Banner data types
      lv_system_time_stamp := f_convert_date(import_rec.system_time_stamp);
      
      lv_doc_code          := SUBSTR(import_rec.doc_code,1,8);
      
      lv_rec_type          := SUBSTR(import_rec.rec_type,1,1);
      
      lv_seq_num           := f_convert_number(import_rec.seq_num);
      
      lv_activity_date     := SYSDATE;
      
      lv_user_id           := SUBSTR(import_rec.user_id,1,30);
      
      lv_rucl_code         := SUBSTR(import_rec.rucl_code,1,4);
      
      lv_doc_ref_num       := SUBSTR(import_rec.doc_ref_num,1,8);
      
      lv_trans_date        := f_convert_date(import_rec.trans_date);
      
      lv_trans_amt         := f_convert_number(import_rec.trans_amt);
      
      lv_trans_desc        := SUBSTR(import_rec.trans_desc,1,35);
      
      lv_dr_cr_ind         := SUBSTR(import_rec.dr_cr_ind,1,1);
      
      lv_acci_code         := SUBSTR(import_rec.acci_code,1,6);
      
      lv_fund_code         := SUBSTR(import_rec.fund_code,1,6);
      
      lv_orgn_code         := SUBSTR(import_rec.orgn_code,1,6);
      
      lv_acct_code         := SUBSTR(import_rec.acct_code,1,6);
      
      lv_prog_code         := SUBSTR(import_rec.prog_code,1,6);
      
      IF NVL(lv_rec_type,'.') = '1' THEN
        
         lv_vendor_pidm    := NULL;
         
         --adjust header record amount
         lv_trans_amt := f_adjust_header_amt(lv_trans_amt, lv_doc_code);
         
      ELSE
        
         IF length(REGEXP_REPLACE(import_rec.vendor_pidm, '[^0-9A-Za-z]', '')) > 8 THEN
           
            lv_vendor_pidm := NULL;
            
         ELSE
      
            lv_vendor_pidm    := f_convert_number(import_rec.vendor_pidm);
            
         END IF;--check for valid pidm
      
      END IF;
      
      --Check to see if the record is a grant
      IF NVL(lv_rec_type,'.') <> '1' AND 
         NVL(SUBSTR(lv_acci_code,1,1),'.') = '5' AND 
         NVL(SUBSTR(lv_fund_code,1,1),'.') = '5' THEN --it's a grant, attempt TRRACCD insert
         
         --Fetch the grant code
         lv_grnt_code := f_get_grnt_code(lv_fund_code);
         
         --Fetch the tran number
         lv_tran_number := f_get_tran_number(lv_grnt_code);
         
         --Derive balance.
         --A positive payment (colun12 = C) equals a negative balance, 
         --a negative payment (column12 = D) equals a positive balance
         IF lv_dr_cr_ind = 'C' THEN
           
            lv_balance := lv_trans_amt * -1;
            
         ELSE
           
            lv_balance := lv_trans_amt;
           
         END IF;
         
         --Check to see if the record exists in TRRACCD already
         SELECT COUNT(*)
           INTO lv_count
           FROM trraccd
          WHERE trraccd.trraccd_desc = lv_doc_code
            AND trraccd.trraccd_detail_code = lv_detail_code;
            
         IF lv_count = 0 THEN
         
            --Attempt TRRACCD insert
            p_insert_to_trraccd(lv_grnt_code,
                                lv_tran_number,
                                lv_coas_code,
                                lv_fund_code,
                                lv_detail_code,
                                lv_trans_date,
                                lv_system_time_stamp,
                                lv_trans_amt,
                                lv_balance,
                                lv_srce_code,
                                lv_acct_feed_ind,
                                lv_session_number,
                                SYSDATE,
                                lv_user_id,
                                lv_doc_code,
                                lv_system_time_stamp);
                                
         ELSE
           
            dbms_output.put_line('Doc Code ' || lv_doc_code || 
                                 ' already exists in TRRACCD. Insert skipped.');
                             
         END IF;--Check to see if the record exists in TRRACCD already
         
      ELSE --it's not a grant, attempt GURFEED insert
        
         --Check to see if the record has already been processed  
         SELECT COUNT(*)
           INTO lv_count_fgbtrni
           FROM fgbtrni
          WHERE fgbtrni.fgbtrni_doc_code = import_rec.doc_code;
           
         SELECT COUNT(*)
           INTO lv_count_fgbjvch
           FROM fgbjvch
          WHERE fgbjvch.fgbjvch_doc_num = import_rec.doc_code;
           
         SELECT COUNT(*)
           INTO lv_count_fgbtrnh
           FROM fgbtrnh
          WHERE fgbtrnh.fgbtrnh_doc_code = import_rec.doc_code;
         
         IF (lv_count_fgbjvch + lv_count_fgbtrnh + lv_count_fgbtrni) = 0 THEN
            
            --The record has not been processed.
            --Attempt insert to GURFEED
            p_insert_to_gurfeed(lv_system_id,
                                lv_system_time_stamp,
                                lv_doc_code,
                                lv_rec_type,
                                lv_submission_number,
                                lv_item_num,
                                lv_seq_num,
                                lv_activity_date,
                                lv_user_id,
                                lv_rucl_code,
                                lv_doc_ref_num,
                                lv_trans_date,
                                lv_trans_amt,
                                lv_trans_desc,
                                lv_dr_cr_ind,
                                lv_coas_code,
                                lv_acci_code,
                                lv_fund_code,
                                lv_orgn_code,
                                lv_acct_code,
                                lv_prog_code,
                                lv_vendor_pidm);
           
         END IF;--check to see if the record has already been processed
         
      END IF;--Check to see if the record is a grant
  
  END LOOP;
  
  --Report totals
  v_tot_ins_count := v_gur_ins_count + v_trr_ins_count;
  
  dbms_output.put_line(' ');
  dbms_output.put_line('Import completed.');
  dbms_output.put_line(' ');
  dbms_output.put_line('Errors reported: ' || v_err_count);
  dbms_output.put_line(' ');
  dbms_output.put_line('GURFEED records deleted : ' || v_gur_del_count);
  dbms_output.put_line('GURFEED records inserted: ' || v_gur_ins_count);
  dbms_output.put_line('TRRACCD records deleted : ' || v_trr_del_count);
  dbms_output.put_line('TRRACCD records inserted: ' || v_trr_ins_count);
  dbms_output.put_line(' ');
  dbms_output.put_line('Total records inserted  : ' || v_tot_ins_count);
  
EXCEPTION
  WHEN OTHERS THEN
    
    dbms_output.put_line('Fatal error occured on record ' || lv_seq_num || 
                         ' during P_IMPORT_FROM_EXT_TABLE: ' || SQLERRM);
    
END P_IMPORT_FROM_EXT_TABLE;

END FIN_RAISERS_EDGE_IMPORT;

/
