 CREATE TABLE "TCAPP"."RE_IMPORT_EXT" 
   (	"SYSTEM_TIME_STAMP" VARCHAR2(50 BYTE), 
	"DOC_CODE" VARCHAR2(50 BYTE), 
	"REC_TYPE" VARCHAR2(50 BYTE), 
	"SEQ_NUM" VARCHAR2(50 BYTE), 
	"ACTIVITY_DATE" VARCHAR2(50 BYTE), 
	"USER_ID" VARCHAR2(50 BYTE), 
	"RUCL_CODE" VARCHAR2(50 BYTE), 
	"DOC_REF_NUM" VARCHAR2(50 BYTE), 
	"TRANS_DATE" VARCHAR2(50 BYTE), 
	"TRANS_AMT" VARCHAR2(50 BYTE), 
	"TRANS_DESC" VARCHAR2(50 BYTE), 
	"DR_CR_IND" VARCHAR2(50 BYTE), 
	"ACCI_CODE" VARCHAR2(50 BYTE), 
	"FUND_CODE" VARCHAR2(50 BYTE), 
	"ORGN_CODE" VARCHAR2(50 BYTE), 
	"ACCT_CODE" VARCHAR2(50 BYTE), 
	"PROG_CODE" VARCHAR2(50 BYTE), 
	"VENDOR_PIDM" VARCHAR2(50 BYTE)
   ) 
   ORGANIZATION EXTERNAL 
    ( TYPE ORACLE_LOADER
      DEFAULT DIRECTORY "RE_IMPORT"
      ACCESS PARAMETERS
      ( RECORDS DELIMITED BY NEWLINE
    FIELDS TERMINATED BY '\t'
    OPTIONALLY ENCLOSED BY '"'
    MISSING FIELD VALUES ARE NULL
    (
     SYSTEM_TIME_STAMP        CHAR(50),
     DOC_CODE                 CHAR(50),
     REC_TYPE                 CHAR(50),
     SEQ_NUM                  CHAR(50),
     ACTIVITY_DATE            CHAR(50),
     USER_ID                  CHAR(50),
     RUCL_CODE                CHAR(50),
     DOC_REF_NUM              CHAR(50),
     TRANS_DATE               CHAR(50),
     TRANS_AMT                CHAR(50),
     TRANS_DESC               CHAR(50),
     DR_CR_IND                CHAR(50),
     ACCI_CODE                CHAR(50),
     FUND_CODE                CHAR(50),
     ORGN_CODE                CHAR(50),
     ACCT_CODE                CHAR(50),
     PROG_CODE                CHAR(50),
     VENDOR_PIDM              CHAR(50)
    )
       )
      LOCATION
       ( 'Post_20180824130250_TEST.txt'
       )
    )
   REJECT LIMIT UNLIMITED ;

